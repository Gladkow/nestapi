import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';
import { Role } from './role.enum';

@Entity()
export class UserEntity{
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ unique: true })
	email: string;

	@Column({ select: false })
	password: string;

	@Column({ type: 'enum', enum: Role, default: Role.OWNER })
	role: Role;
}