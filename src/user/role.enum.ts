export enum Role {
  OWNER = 'owner',
  MANAGER = 'manager',
  ADMIN = 'admin',
}
