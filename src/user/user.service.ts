import { Injectable } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { DeleteResult, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, ObservedValueOf } from 'rxjs';
import { UserInterface } from './user.interface';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(UserEntity)
		private readonly repo: Repository<UserEntity>,
	) {}

	createPost(user: UserInterface): Observable<UserInterface> {
		return from(this.repo.save(user));
	}

	findAllPosts(): Observable<UserInterface[]> {
		return from(this.repo.find());
	}

	findUser(id: number): Observable<ObservedValueOf<Promise<UserEntity | null>>> {
		return from(this.repo.findOne({ where: { id: id } }));
	}

	deleteUser(id: number): Observable<DeleteResult> {
		return from(this.repo.delete(id));
	}
}