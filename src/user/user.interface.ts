import { Role } from './role.enum';

export interface UserInterface {
	name: string;
	email: string;
	password: string;
	role: Role;
}