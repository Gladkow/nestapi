import { Entity } from 'typeorm';
import { Role } from './role.enum';

@Entity()
export class UserModel {
	id: number;
	email: string;
	password: string;
	role: Role;

}