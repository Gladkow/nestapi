import { Controller, HttpCode, Body, Post, Get, Request, Param, Delete, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { Observable, ObservedValueOf } from 'rxjs';
import { UserInterface } from './user.interface';
import { UserEntity } from './user.entity';
import { DeleteResult } from 'typeorm';
import { JwtGuard } from '../auth/guards/jwt.guard';

@Controller('user')
export class UserController {
	constructor(private readonly userService: UserService) {
	}

	@Post()
	create(@Body() user: UserInterface, @Request() req): Observable<UserInterface> {
		return this.userService.createPost(user);
	}

	@UseGuards(JwtGuard)
	@Get()
	findAll(): Observable<UserInterface[]> {
		return this.userService.findAllPosts();
	}

	@Get(':id')
	findUser(@Param('id') id: number): Observable<ObservedValueOf<Promise<UserEntity | null>>> {
		return this.userService.findUser(id);
	}

	@Delete(':id')
	delete(@Param('id') id: number): Observable<DeleteResult> {
		return this.userService.deleteUser(id);
	}
}
