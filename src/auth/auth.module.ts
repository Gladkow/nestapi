import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

import { AuthService } from '../services/auth.service';
import { AuthController } from '../auth/controllers/auth.controller';
import { UserEntity } from '../user/user.entity';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { JwtStrategy } from '../auth/guards/jwt.strategy';
import { RolesGuard } from '../auth/guards/roles.guard';
import { UserService } from '../user/user.service';
import { UserController } from '../user/user.controller';

@Module({
	imports: [
		JwtModule.registerAsync({
			useFactory: () => ({
				secret: process.env.JWT_SECRET,
				signOptions: { expiresIn: '3600s' },
			}),
		}),
		TypeOrmModule.forFeature([UserEntity]),
	],
	providers: [AuthService, JwtGuard, JwtStrategy, RolesGuard, UserService],
	controllers: [AuthController, UserController],
	exports: [AuthService, UserService],
})
export class AuthModule {
}
