import { AuthDto } from './dto/auth.dto';
export declare class AuthController {
    regiser(dto: AuthDto): Promise<void>;
    login(dto: AuthDto): Promise<string>;
}
