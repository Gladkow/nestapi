export declare class users {
    id: number;
    name: string;
    lastName: string;
    isActive: boolean;
}
