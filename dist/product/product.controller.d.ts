import { ProductModule } from "./product.module";
import { FindProductDto } from "./dto/find-product.dto";
export declare class ProductController {
    create(dto: Omit<ProductModule, '_id'>): Promise<void>;
    get(id: string): Promise<void>;
    delete(id: string): Promise<void>;
    patch(id: string, dto: ProductModule): Promise<void>;
    find(dto: FindProductDto): Promise<void>;
}
