"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.databaseProviders = void 0;
const typeorm_1 = require("typeorm");
const config_1 = require("@nestjs/config");
const path_1 = require("path");
exports.databaseProviders = [
    {
        provide: 'DATABASE_CONNECTION',
        inject: [config_1.ConfigService],
        useFactory: async (configService) => (0, typeorm_1.createConnection)({
            type: 'postgres',
            host: configService.get('POSTGRES_HOST'),
            port: configService.get('POSTGRES_PORT'),
            username: configService.get('POSTGRES_USER'),
            password: configService.get('POSTGRES_PASSWORD'),
            database: configService.get('POSTGRES_DB'),
            entities: [path_1.default.join(__dirname, '**/*.entity.{ts,js}')],
            synchronize: true,
        }),
    },
];
//# sourceMappingURL=database.providers.js.map