export declare enum TopLevelCategory {
    Coureses = 0,
    Services = 1,
    Books = 2,
    Products = 3
}
export declare class TopPageModel {
    _id: string;
    firstLevelCategory: TopLevelCategory;
    secondCategory: string;
    title: string;
    category: string;
    hh?: {
        count: number;
        juniorSalary: number;
        middleSalary: number;
        seniorSalary: number;
    };
    advatages: {
        title: string;
        description: string;
    }[];
    seoText: string;
    tagsTitle: string;
    tags: string[];
}
