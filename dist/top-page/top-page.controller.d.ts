import { TopPageModule } from './top-page.module';
import { FindTopPageDto } from './dto/find-top-page.dto';
export declare class TopPageController {
    create(dto: Omit<TopPageModule, '_id'>): Promise<void>;
    get(id: string): Promise<void>;
    delete(id: string): Promise<void>;
    patch(id: string, dto: TopPageModule): Promise<void>;
    find(dto: FindTopPageDto): Promise<void>;
}
