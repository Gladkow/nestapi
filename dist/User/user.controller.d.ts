import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { UserInterface } from './user.interface';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    create(user: UserInterface, req: any): Observable<UserInterface>;
}
