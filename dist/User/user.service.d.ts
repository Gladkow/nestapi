import { User } from './user.entity';
import { Repository } from 'typeorm';
import { Observable } from 'rxjs';
import { UserInterface } from './user.interface';
export declare class UserService {
    private readonly repo;
    constructor(repo: Repository<User>);
    createPost(user: UserInterface): Observable<UserInterface>;
}
